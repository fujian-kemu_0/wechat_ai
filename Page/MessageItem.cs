﻿using FlaUI.WinForm.UIAuto.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FlaUI.WinForm.Page.MainPage.User
{
    public partial class MessageItem : UserControl
    {
        public MessageItem()
        {
            InitializeComponent();
        }
        WeChatContractMessageDto current = null;
        public void SetMessage(WeChatContractMessageDto message)
        {
            if (message == null)
            {
                return;
            }
            this.Tag = message;
            //   this.Height = 16;
            current = message; lbImage.Visible = false;
            //if (message.InfoType == 0)
            //{
            //}
            if (message.UserType == 3)
            {
               // pictureBox1.Image = Resources.时间;
                lbMessage.Text = "时间:" + message.Message;
                lbMessage.ForeColor = Color.Red;
            }
            else
            {
                if (message.UserType == 1)
                {
                    lbMessage.ForeColor = Color.Orange;
                }
                if (message.UserType == 2)
                {
                    lbMessage.ForeColor = Color.Blue;
                }
                lbMessage.Text = message.UserName + ":" + message.Message;
            }
            if (message.UserHeader != null)
            {
                pictureBox1.Image = message.UserHeader;
            }
        }

        private void lbImage_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //ShowElementImage image = new ShowElementImage();
            //image.SetImage(current.ImageBitMap);
            //image.Show();
        }
    }
}
