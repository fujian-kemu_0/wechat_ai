﻿using AllData;
using FlaUI.WinForm.Page.MainPage.User;
using FlaUI.WinForm.UIAuto.Model;
using OnlineRetailers.Extension.Page.UIAuto;
using OnlineRetailers.Extension.Page.UIAuto.Business.WX.Task;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.UIAuto.mdb;
using FlaUI.WinForm.UIAuto.Business.WX.Task.Base;

namespace FlaUI.WinForm.Page.MainPage
{
    public partial class WeCharRealMessage : Form
    {
        public static List<Form_cls> form_list = new List<Form_cls>();   /////窗口类参数
        public static system_cls sys = new system_cls();
        public static string fz = "";

        public WeCharRealMessage()
        {
            InitializeComponent();
            Currnet = this;
            this.Load += WeCharRealMessage_Load;
        }
        public static WeCharRealMessage Currnet { get; internal set; }
        private void WeCharRealMessage_Load(object sender, EventArgs e)
        {
            this.Text = sys.title;
            load_group_list();
            UI_WX_Window.Current.Load();
        }

        UI_WX_RealMsg_Listen_Task task = null;

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (btnStart.Text == "开始")
            {
                task = new UI_WX_RealMsg_Listen_Task();
                task.AddRealWeChatWindow += Task_AddRealWeChatWindow;
                task.RemoveRealWeChatWindow += Task_RemoveRealWeChatWindow;
                task.WeChatMessageRemind += Task_WeChatMessageRemind;
                task.Start();
                btnStart.Text = "停止";
            }
            else
            {
                btnStart.Text = "开始";

                task.Stop();
            }
        }

        private void Task_WeChatMessageRemind(object sender, WeChatMessageRemindEventArgs e)
        {
            Invoke(new Action(() =>
            {
                foreach (var item in panels)
                {
                    if ((item).Tag == e.ChatWindowDto)
                    {
                        MessageItem messageItem = new MessageItem();
                        messageItem.SetMessage(e.NewMessage);
                        item.Controls.Add(messageItem);
                        break;

                    }
                }
            }));
        }

        private void Task_RemoveRealWeChatWindow(object sender, WeChatWindowHandlerEventArgs e)
        {
            RemoveTabPage(e.ChatWindowDto);
        }

        private void Task_AddRealWeChatWindow(object sender, WeChatWindowHandlerEventArgs e)
        {
            AddTabPage(e.ChatWindowDto);
        }

        List<System.Windows.Forms.FlowLayoutPanel> panels = new List<FlowLayoutPanel>();


        private void load_group_list()
        {
            DataLink dl = new DataLink();
            DataSet ds = new DataSet();
            form_list.Clear();
            ds = dl.getData("select * from t_group");
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    Form_cls fs= new Form_cls();
                    fs.group_id = ds.Tables[0].Rows[i]["group_id"].ToString();
                    fs.group_name= ds.Tables[0].Rows[i]["group_name"].ToString();
                    if (ds.Tables[0].Rows[i]["monitor_type"].ToString().Substring(0, 1)=="1")
                    {
                        fs.is_log = true;
                    }
                    if (ds.Tables[0].Rows[i]["monitor_type"].ToString().Substring(1, 1) == "1")
                    {
                        fs.is_sen = true;
                    }
                    if (ds.Tables[0].Rows[i]["monitor_type"].ToString().Substring(2, 1) == "1")
                    {
                        fs.is_ai = true;
                    }
                   // fs.sens_str.Add((string)("完蛋了"));
                    form_list.Add(fs);
                }
            }
        }

        private void AddTabPage(WeChatWindowsDto dto)//////添加微信聊天窗口
        {
            Invoke(new Action(() =>
            {
                System.Windows.Forms.TabPage tab = new TabPage();
                tab.Location = new System.Drawing.Point(4, 26);
                tab.Padding = new System.Windows.Forms.Padding(3);
                tab.Size = new System.Drawing.Size(742, 19);
                tab.Text = dto.ChatUser;
                tab.UseVisualStyleBackColor = true;
                this.tabMessage.Controls.Add(tab);

                FlowLayoutPanel panel = new FlowLayoutPanel();
                panel.FlowDirection = FlowDirection.TopDown;
                panel.WrapContents = true;
                panel.AutoScroll = true;

                panel.Dock = System.Windows.Forms.DockStyle.Fill;
                panel.Tag = dto;
                tab.Controls.Add(panel);
                panels.Add(panel);
            }));
        }

        private void RemoveTabPage(WeChatWindowsDto dto)  ////删除聊天窗口
        {
            Invoke(new Action(() =>
            {
                foreach (var item in panels)
                {
                    if ((item).Tag == dto)
                    {
                        panels.Remove(item);
                        break;
                    }
                }
                foreach (TabPage item in tabMessage.Controls)
                {
                    if ((item).Text == dto.ChatUser)
                    {
                        tabMessage.Controls.Remove(item);
                        break;
                    }
                }
            }));
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            load_group_list();
        }

        private void WeCharRealMessage_Load_1(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (DateTime.Now.ToString("mm") == fz)
                {
                    return;
                }
                else
                {
                    fz = DateTime.Now.ToString("mm");
                }
                DataLink dl = new DataLink();
                DataSet ds = new DataSet();
                string wechat_mes = "";
                string zname = "";
                string zphone = "";
                if (DateTime.Now.ToString("HH:mm") == "09:30")
                {
                    ds = dl.getData("select * from t_zbb where dat='" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        wechat_mes = "【国家电网晋江公司】迎峰度夏安全执规值班提示：\r\n【" + DateTime.Now.ToString("yyyy-MM-dd")+"】\r\n值班领导："+ ds.Tables[0].Rows[0]["leader"].ToString() + "\r\n值班中层："+ ds.Tables[0].Rows[0]["middle"].ToString()+"  "+ ds.Tables[0].Rows[0]["middle_ph"].ToString() + " \r\n值班专责:  " + ds.Tables[0].Rows[0]["specific"].ToString() +"  "+ ds.Tables[0].Rows[0]["specific_ph"].ToString() + "\r\n";
                    }
                    ds = dl.getData("select * from t_zbb where dat='" + DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") + "'"); 
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        wechat_mes = wechat_mes+ "【" + DateTime.Now.AddDays(1).ToString("yyyy-MM-dd") + "】\r\n值班领导：" + ds.Tables[0].Rows[0]["leader"].ToString() + "\r\n值班中层：" + ds.Tables[0].Rows[0]["middle"].ToString() + "  " + ds.Tables[0].Rows[0]["middle_ph"].ToString() + " \r\n值班专责:  " + ds.Tables[0].Rows[0]["specific"].ToString() + "  " + ds.Tables[0].Rows[0]["specific_ph"].ToString() + "\r\n";
                    }
                    ds = dl.getData("select * from t_zbb where dat='" + DateTime.Now.AddDays(2).ToString("yyyy-MM-dd") + "'");
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        wechat_mes = wechat_mes + "【" + DateTime.Now.AddDays(2).ToString("yyyy-MM-dd") + "】\r\n值班领导：" + ds.Tables[0].Rows[0]["leader"].ToString() + "\r\n值班中层：" + ds.Tables[0].Rows[0]["middle"].ToString() + "  " + ds.Tables[0].Rows[0]["middle_ph"].ToString() + " \r\n值班专责:  " + ds.Tables[0].Rows[0]["specific"].ToString() + "  " + ds.Tables[0].Rows[0]["specific_ph"].ToString() + "\r\n";
                    }
                    com.send_mes_byname("周末安全巡查及迎峰度夏值班", "0", wechat_mes);
                }
                wechat_mes = "";
                if (DateTime.Now.ToString("HH:mm") == "08:10")
                {
                    ds = dl.getData("select * from t_czs where dat='" + DateTime.Now.ToString("yyyy-MM-dd") + "'");
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        wechat_mes = "【国家电网晋江公司】磁灶供电所值班提示：\r\n【" + DateTime.Now.ToString("yyyy-MM-dd") + "】\r\n 值班领导：" + ds.Tables[0].Rows[0]["leader"].ToString()+ "  \r\n 电话：" + ds.Tables[0].Rows[0]["leader_phone"].ToString()+ "\r\n值班负责人：" + ds.Tables[0].Rows[0]["fuze"].ToString() + "  \r\n 电话：" + ds.Tables[0].Rows[0]["fuze_phone"].ToString() + " \r\n接单人员:  " + ds.Tables[0].Rows[0]["order"].ToString() + "  \r\n 电话：" + ds.Tables[0].Rows[0]["order_phone"].ToString() + " \r\n值班人员:  "+ ds.Tables[0].Rows[0]["people"].ToString();
                    }
                    com.send_mes_byname("磁灶抢修群", "0", wechat_mes);
                }
            }
            catch { }
        }
    }
}
