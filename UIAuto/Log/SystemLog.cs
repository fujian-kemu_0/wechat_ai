﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineRetailers.Extension.Page.UIAuto.Log
{
    /// <summary>
    /// 系统日志
    /// </summary>
    public class SystemLog
    {
        private static IWXLog Current { get; set; }

        public static void Load(IWXLog current)
        {
            Current = current;
        }

        public static void Info(string messgae)
        {
            if (Current != null)
            { Current.Log(messgae); }
        }
        public static void Error(string messgae)
        {
            if (Current != null)
            { Current.Error(messgae); }
        }

    }
}
