﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineRetailers.Extension.Page.UIAuto
{
   public interface IWXLog
    {
        void Log(string msg);
        void Error(string msg);
    }
}
