﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaUI.WinForm.UIAuto.Model
{
    /// <summary>
    /// 微信聊天消息模型
    /// </summary>
    public class WeChatContractMessageDto
    {
        public WeChatContractMessageDto() {
            MessageType = 0;
        }
        /// <summary>
        /// 消息内容
        /// </summary>
        public string Message { get; set; }
        /// <summary>
        /// 0消息 1图片 2动画表情
        /// </summary>
        public int MessageType { get; set; }

        /// <summary>
        /// 图片二进制对象
        /// </summary>
        public Bitmap ImageBitMap { get; set; }

        /// <summary>
        /// 用户头像（准确度不行）
        /// </summary>
        public Bitmap UserHeader { get; set; }

        /// <summary>
        /// 时间
        /// </summary>
        public DateTime Time { get; set; }

        /// <summary>
        /// 1发送方 2接收方 3时间 
        /// </summary>
        public int UserType { get; set; }

        /// <summary>
        /// 微信聊天用户
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// 消息处理状态
        /// </summary>
        public MessageExecuteStatus ExecuteStatus { get; set; }

        /// <summary>
        /// 上一条消息（链路结构）
        /// </summary>
        public WeChatContractMessageDto PreMessage { get; set; }

        public static bool Equals(WeChatContractMessageDto t1, WeChatContractMessageDto t2)
        {
            if (t1 == null && t2 == null) return true;

            if (t1 == null || t2 == null) return false;

            if (t1 != null && t2 != null)
            {

                return t1.UserName == t2.UserName && t1.Message == t2.Message;

            }
            return false;
        }
    }
    /// <summary>
    /// 微信单个对象聊天集合
    /// </summary>
    public class WeChatWindowsDto
    {
        public WeChatWindowsDto()
        {
            Messages = new List<WeChatContractMessageDto>();
            Init = false;
        }
        public string ChatUser { get; set; }
        /// <summary>
        /// 是否初始化历史消息
        /// </summary>
        public bool Init { get; set; }
        /// <summary>
        /// 消息列表
        /// </summary>
        public List<WeChatContractMessageDto> Messages { get; set; }
    }

    /// <summary>
    /// 消息处理状态
    /// </summary>
    public enum MessageExecuteStatus
    {
        /// <summary>
        /// 未处理
        /// </summary>
        Untreated,
        /// <summary>
        /// 处理完成
        /// </summary>
        Complete,
    }
}
