﻿using OnlineRetailers.Extension.Page.UIAuto.Log;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineRetailers.Extension.Page.UIAuto.Model
{
    public class WXSendMessageSource : List<WeChatInputMsgEntity>
    {

        private static WXSendMessageSource source = null;

        public static WXSendMessageSource Current
        {
            get
            {
                if (source == null)
                    source = new WXSendMessageSource();
                return source;
            }
        }


    }
    /// <summary>
    /// 微信消息输入数据结构
    /// </summary>
    public class WeChatInputMsgEntity
    {
        public WXMessageType Type { get; set; }

        public string TypeName
        {
            get
            {
                switch (Type)
                {
                    case WXMessageType.Text: return "文本";
                    case WXMessageType.File: return "文件";
                    case WXMessageType.Image: return "图片";
                }
                return "";
            }
        }

        public string Info { get; set; }

        public void Copy()
        {
            try
            {
                switch (Type)
                {
                    case WXMessageType.Text:
                        Clipboard.SetText(Info);
                        break;
                    case WXMessageType.Image:
                        if (File.Exists(Info))
                            Clipboard.SetImage(Image.FromFile(Info));
                        else
                            SystemLog.Error("没有找到" + Info);
                        break;
                    case WXMessageType.File:
                        if (File.Exists(Info))
                        {
                            var files = new System.Collections.Specialized.StringCollection();
                            files.Add(Info);
                            Clipboard.SetFileDropList(files);
                        }
                        else
                            SystemLog.Error("没有找到" + Info);
                        break;
                }
            }
            catch { }
        }
    }

    public enum WXMessageType
    {
        /// <summary>
        /// 文本
        /// </summary>
        Text = 0,
        /// <summary>
        /// 微信图片
        /// </summary>
        Image = 1,
        /// <summary>
        /// 文件
        /// </summary>
        File = 2
    }
}
