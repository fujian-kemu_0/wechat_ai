﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineRetailers.Extension.Page.UIAuto.Model
{
    /// <summary>
    /// 任务状态
    /// </summary>
    public enum TaskState
    {
        /// <summary>
        /// 正在运行
        /// </summary>
        Runing = 0,
        /// <summary>
        /// 停止
        /// </summary>
        Stop = 1,
        /// <summary>
        /// 开始
        /// </summary>
        Start = 2,
        /// <summary>
        /// 正在停止
        /// </summary>
        Stoping = 3,
        /// <summary>
        /// 完成
        /// </summary>
        Completed = 4,
    }
}
