﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OnlineRetailers.Extension.Page.UIAuto
{


    /// <summary>
    /// 微信联系人
    /// </summary>
   public class WXContactEntity
    {
        public string Name { get; set; }
    }
    public class WXContects : List<WXContactEntity>
    {
        public void Add(string name)
        {
            this.Add(new WXContactEntity { Name=name });
        }

        public bool Contains(string name)
        {
            return this.FirstOrDefault(item => item.Name == name) != null;
        }
    }
}
