﻿using FlaUI.Core.AutomationElements;
using FlaUI.Core.Input;
using FlaUI.Core.WindowsAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OnlineRetailers.Extension.Page.UIAuto.Business.WIN
{
  public  class WXWindowMessage
    {
        /// <summary>
        /// 实现静默点击向窗口发送windows消息
        /// </summary>
        /// <param name="ele"></param>
        public static void Click(AutomationElement ele)
        {
            if (ele == null)
            {
                Log.SystemLog.Info("点击事件元素为空");
                return;
            }

            ele.Focus();

            var point = ele.GetClickablePoint();

            var windowsRectangle = UI_WX_Window.Current.Window.BoundingRectangle;

            var handler = UI_WX_Window.Current.Window.Properties.NativeWindowHandle.ValueOrDefault;

            int x = point.X - windowsRectangle.X;
            int y = point.Y - windowsRectangle.Y;

            IntPtr lParam = (IntPtr)((y << 16) + x);

            IntPtr wParam = IntPtr.Zero;

            User32.SendMessage(handler, WindowsMessages.WM_LBUTTONDOWN, wParam, lParam);
            User32.SendMessage(handler, WindowsMessages.WM_LBUTTONUP, wParam, lParam);
            Log.SystemLog.Info("向元素【" + ele.Name + "】发送win消息成功");
        }
    }
}
