﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OnlineRetailers.Extension.WXAuto.Page.MainPage
{
	public struct WindowInfo
	{
		public IntPtr hWnd;
		public string szWindowName;
		public string szClassName;
	}
	public class WXWin32
	{
		
		private static IntPtr currentWeixin = IntPtr.Zero;

        public static IntPtr GetWeiXin()
        {
            //if (currentWeixin == IntPtr.Zero)
            currentWeixin = Win32_Find_Window.Do("WeChatMainWndForPC", "微信");    ////WeChatMainWndForPC为微信类名---获取句柄
            return currentWeixin;
        }
		private delegate bool WNDENUMPROC(IntPtr hWnd, int lParam);
		//用来遍历所有窗口 
		[DllImport("user32.dll")]
		private static extern bool EnumWindows(WNDENUMPROC lpEnumFunc, int lParam);

		//获取窗口Text 
		[DllImport("user32.dll")]
		private static extern int GetWindowTextW(IntPtr hWnd, [MarshalAs(UnmanagedType.LPWStr)] StringBuilder lpString, int nMaxCount);

		//获取窗口类名 
		[DllImport("user32.dll")]
		private static extern int GetClassNameW(IntPtr hWnd, [MarshalAs(UnmanagedType.LPWStr)] StringBuilder lpString, int nMaxCount);

		public static WindowInfo[] GetAllDesktopWindows()
		{
			//用来保存窗口对象 列表
			List<WindowInfo> wndList = new List<WindowInfo>();

			//enum all desktop windows 
			EnumWindows(delegate (IntPtr hWnd, int lParam)
			{
				WindowInfo wnd = new WindowInfo();
				StringBuilder sb = new StringBuilder(256);

				//get hwnd 
				wnd.hWnd = hWnd;

				//get window name  
				GetWindowTextW(hWnd, sb, sb.Capacity);
				wnd.szWindowName = sb.ToString();

				//get window class 
				GetClassNameW(hWnd, sb, sb.Capacity);
				wnd.szClassName = sb.ToString();

				//add it into list 
				wndList.Add(wnd);
				return true;
			}, 0);

			return wndList.ToArray();
		}
	}


	public class Win32_ShowWindow
    {
        private const int iRestore = 9;
        private const int iShow = 6;
        private const int SW_SHOWMAXIMIZED = 3;
        

        [DllImport("user32.dll")]
        public static extern int ShowWindow(IntPtr Hwnd, int iCmdShow);
        public static void Min(IntPtr hwnd)
        {
            ShowWindow(hwnd, iShow);
        }
        public static void Max(IntPtr hwnd)
        {
            ShowWindow(hwnd, SW_SHOWMAXIMIZED);
        }

        public static void Normal(IntPtr hwnd)
        {
            ShowWindow(hwnd, iRestore);
        }
    }
    /// <summary>
    /// 是否曾献在window中
    /// </summary>
    public class Win32_IsWindowVisible
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool IsWindowVisible(IntPtr hWnd);

        public static bool Do(IntPtr hWnd)
        {
            if (hWnd != IntPtr.Zero)
                return IsWindowVisible(hWnd);
            return true;
        }
    }
     
    /// <summary>
    /// 设置焦点
    /// </summary>
    public class Win32_SetForegroundWindow
    {

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool SetForegroundWindow(IntPtr hWnd);
        public static void Do(IntPtr hWnd)
        {
            if (hWnd != IntPtr.Zero)
                SetForegroundWindow(hWnd);
        }
    }
    /// <summary>
    /// 鼠标移动
    /// </summary>
    public class Win32_Move_Mouse
    {

        [DllImport("user32.dll")]
        static extern bool SetCursorPos(int X, int Y);
        public static void Do(int x, int y)
        {
            SetCursorPos(x, y);
        }
    }
    /// <summary>
    /// 发现窗口
    /// </summary>
    public class Win32_Find_Window
    {
        [DllImport("user32.dll", EntryPoint = "FindWindow")]
        private extern static IntPtr FindWindow(string lpClassName, string lpWindowName);
        public static IntPtr Do(string className, string windowName)
        {
            return FindWindow(className, windowName);
        }
    }
}
