﻿using OnlineRetailers.Extension.Page.UIAuto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FlaUI.Core.AutomationElements;
using OnlineRetailers.Extension.Page.UIAuto.Model;
using FlaUI.WinForm.Page.MainPage;

namespace FlaUI.WinForm.UIAuto.Business.WX.Task.Base
{
    /// <summary>
    /// 微信自动化任务基础元素操控
    /// </summary>
    public class UI_WX_Element_Base_Task : UI_WX_Base_Task
    {
        public AutomationElement Search { get; set; }

        /// <summary>
        /// 获取搜索框元素
        /// </summary>
        protected void GetSearchElement()
        {
            if (Search == null)
                Search = UI_WX_Window.Current.Find("/Pane/Pane[1]/Pane[1]/Pane[1]/Text");
        }
        /// <summary>
        /// 获取内容搜索框列举出来的搜索结果列表内容
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected AutomationElement GetSearchResultElement(string name)
        {
            for (int i = 0; i < 15; i++)
            {
                var currentConract = UI_WX_Window.Current.Find("/Pane/Pane[1]/Pane[2]/Pane[2]/List/ListItem[1]");

                if (currentConract != null
                    &&
                    currentConract.Name == name)
                {

                    return currentConract;
                }
                Thread.Sleep(100);
            }
            return null;
        }

        /// <summary>
        /// 获取搜索好友搜索框列举出来的搜索结果列表内容
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        protected AutomationElement GetSearchFriendResultElement(string name)
        {
            for (int i = 0; i < 15; i++)
            {
                var currentConract = UI_WX_Window.Current.Find("/Pane/Pane[1]/Pane[2]/Pane/List/ListItem");

                if (currentConract != null
                    &&
                    currentConract.Name == name)
                {

                    return currentConract;
                }
                Thread.Sleep(100);
            }
            return null;
        }


        /// <summary>
        /// 获取微信发送内容的输入文本框
        /// </summary>
        /// <returns></returns>
        protected AutomationElement WeChatMsgInputText() {
            var SendInput = UI_WX_Window.Current.Find("/Pane/Pane[2]/Pane/Pane/Pane/Pane/Pane[2]/Pane[2]/Pane[2]/Pane/Pane[1]/Edit");
            return SendInput;
        }
        /// <summary>
        /// 获取微信消息发送按钮
        /// </summary>
        /// <returns></returns>
        protected AutomationElement WeChatMsgSendButton()
        {
            var SendButton = UI_WX_Window.Current.Find("/Pane/Pane[2]/Pane/Pane/Pane/Pane/Pane[2]/Pane[2]/Pane[2]/Pane/Pane[2]/Pane[3]/Button");
            return SendButton;
        }


		/// <summary>
		/// 进行单击设置文本输入框的值 
		/// </summary>
		/// <param name="text"></param>
		/// <param name="ele"></param>
		public static void ClickSetInputValue(WeChatInputMsgEntity msg, AutomationElement ele, bool isClear,bool systemClick=false)
        {
            if (systemClick)
                ele.Click();
            else
                UI_WX_Window.Current.ClickElement(ele);
            Thread.Sleep(100);
            if (isClear)
            {
                UI_WX_Window.Current.ClearInput();
            }
            Thread.Sleep(100);

            WeCharRealMessage.Currnet.Invoke(new Action(() =>
            {
                msg.Copy();
            }));
            Thread.Sleep(100);

            UI_WX_Window.Current.Paste();
        }
		/// <summary>
		/// 进行双击设置文本输入框的值 
		/// </summary>
		/// <param name="msg"></param>
		/// <param name="ele"></param>
		/// <param name="isClear"></param>
		protected void DoubleClickSetInputValue(WeChatInputMsgEntity msg, AutomationElement ele, bool isClear)
        {
            ele.DoubleClick();
            if (isClear)
            {
                UI_WX_Window.Current.ClearInput();
            }
            WeCharRealMessage.Currnet.Invoke(new Action(() =>
            {
                msg.Copy();
            }));

            UI_WX_Window.Current.Paste();
        }
		/// <summary>
		///进行单击设置文本输入框的值 
		/// </summary>
		/// <param name="msg"></param>
		/// <param name="ele"></param>
		protected void ClickSetInputValue(WeChatInputMsgEntity msg, AutomationElement ele)
        {
            ClickSetInputValue(msg, ele, true);
        }
    }
}
