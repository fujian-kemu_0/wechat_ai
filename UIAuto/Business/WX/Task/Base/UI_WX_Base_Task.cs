﻿using OnlineRetailers.Extension.Page.UIAuto.Log;
using OnlineRetailers.Extension.Page.UIAuto.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlaUI.WinForm.UIAuto.Business.WX.Task.Base
{
	/// <summary>
	/// 任务执行过程中产生的消息
	/// </summary>
	public class TaskMsgEventArgs : EventArgs
	{
		public string Message { get; set; }
	}

	/// <summary>
	/// 微信自动化任务基类
	/// </summary>
	public abstract class UI_WX_Base_Task
	{
		public UI_WX_Base_Task() { State = TaskState.Stop; }
		/// <summary>
		/// 当前任务状态
		/// </summary>
		public TaskState State { get; protected set; }
		/// <summary>
		/// 任务完成事件
		/// </summary>
		public event EventHandler TaskCompeletedEvent;
		/// <summary>
		/// 任务消息提醒
		/// </summary>
		public event EventHandler<TaskMsgEventArgs> TaskMsgEvent;
		/// <summary>
		/// 设置任务消息
		/// </summary>
		/// <param name="message"></param>
		protected void InputTaskMessage(string message)
		{
			if (TaskMsgEvent != null)
			{
				TaskMsgEvent(this, new TaskMsgEventArgs { Message = message });
			}
		}
		private Stopwatch taskExecuteWatch = new Stopwatch();
		/// <summary>
		/// 开始任务执行（用于统计时间）
		/// </summary>
		protected void StartWatchTaskExecuteTime()
		{
			taskExecuteWatch = new Stopwatch();
			taskExecuteWatch.Start();
		}

		/// <summary>
		/// 结束任务执行(用于统计时间)
		/// </summary>
		protected void EndWatchTaskExecuteTime()
		{
			taskExecuteWatch.Stop();
			SystemLog.Info("执行时间" + TaskExcuteTime + "秒");

		}
		/// <summary>
		/// 执行时间（秒）
		/// </summary>
		public int TaskExcuteTime { get { return (int)taskExecuteWatch.Elapsed.TotalSeconds; } }

		/// <summary>
		/// 启动任务
		/// </summary>
		public virtual void Start()
		{
			StartWatchTaskExecuteTime();
		}
		/// <summary>
		/// 结束任务
		/// </summary>
		public virtual void Stop()
		{
			EndWatchTaskExecuteTime();
			State = TaskState.Stop;
		}
		/// <summary>
		/// 任务完成通知
		/// </summary>
		protected void TaskExecuteCompleted()
		{
			if (TaskCompeletedEvent != null)
			{
				TaskCompeletedEvent(this, EventArgs.Empty);
			}
		}
	}
}
