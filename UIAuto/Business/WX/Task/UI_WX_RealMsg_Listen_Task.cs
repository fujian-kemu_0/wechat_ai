﻿using FlaUI.WinForm.UIAuto.Business.WX.Task.Base;
using FlaUI.WinForm.UIAuto.Model;
using OnlineRetailers.Extension.Page.UIAuto.Log;
using OnlineRetailers.Extension.Page.UIAuto.Model;
using OnlineRetailers.Extension.WXAuto.Page.MainPage;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using AllData;
using FlaUI.Core.AutomationElements;
using System.Net.Http;
using System.IO;
using System.Net;
using System.Collections;
using WindowsFormsApp1.UIAuto.mdb;

namespace OnlineRetailers.Extension.Page.UIAuto.Business.WX.Task
{
    /// <summary>
    /// 针对单独聊天窗口的微信实时聊天消息监听任务
    /// </summary>
    internal class UI_WX_RealMsg_Listen_Task : UI_WX_Element_Base_Task
    {
        /// <summary>
        /// 实时消息的汇总
        /// </summary>
        public List<WeChatWindowsDto> Reals { get; set; }
        /// <summary>
        /// 新增监听窗体
        /// </summary>
        public event EventHandler<WeChatWindowHandlerEventArgs> AddRealWeChatWindow;
        /// <summary>
        /// 移除监听窗体
        /// </summary>
        public event EventHandler<WeChatWindowHandlerEventArgs> RemoveRealWeChatWindow;
        /// <summary>
        /// 新消息提醒
        /// </summary>
        public event EventHandler<WeChatMessageRemindEventArgs> WeChatMessageRemind;

        /// <summary>
        /// 新消息提醒
        /// </summary>
        /// <param name="dto"></param>
        /// <param name="message"></param>
        public void ExecuteWeChatMessageRemind(WeChatWindowsDto dto, WeChatContractMessageDto message)  /////当前消息处理
        {
            try
            {
                Form_cls group_oject=new Form_cls();
                if (WeChatMessageRemind != null)
                {
                    WeChatMessageRemind(this, new WeChatMessageRemindEventArgs { ChatWindowDto = dto , NewMessage=message});
                    string msg = "";
                        if(message.UserName== "数字Ai助手")
                        {
                           return;
                        }
                        DataLink dl = new DataLink();
                        msg = message.Message.ToString();
                        if (msg.Length > 30)
                        {
                            msg = msg.Substring(0, 30);
                        }
                            group_oject = com.get_group_oject_byname(dto.ChatUser);
                            if (group_oject != null)
                            {
                                if (group_oject.group_id != "" && group_oject.is_log)
                                {
                                    //dl.SetUpdate("insert into t_log values('" + group_oject.group_id + "','" + message.UserName + "','0','" + message.MessageType + "','" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + message.Message.ToString() + "')");
                                }
                            }                       
                }
            }
            catch (Exception ex)
            {
                DataLink.Writelog(ex.ToString());
            }
        }

        public void ExecuteAddWindowReal(WeChatWindowsDto dto)
        {
            if (AddRealWeChatWindow != null)
            {
                AddRealWeChatWindow(this, new WeChatWindowHandlerEventArgs { ChatWindowDto = dto });
            }
        }
        public void ExecuteRemoveWindowReal(WeChatWindowsDto dto)
        {
            if (RemoveRealWeChatWindow != null)
            {
                RemoveRealWeChatWindow(this, new WeChatWindowHandlerEventArgs { ChatWindowDto = dto });
            }
        }

        public UI_WX_RealMsg_Listen_Task()
        {
            Reals = new List<WeChatWindowsDto>();
        }

        public override void Start()
        {
            base.Start();

            State = Model.TaskState.Start;

            Reals.Clear();

            Thread th = new Thread(new ThreadStart(() =>
            {
                State = Model.TaskState.Runing;

                while (State == TaskState.Runing)
                {
					var weChatWindows = UI_WX_Window.Current.GetWeChatWindows();
                    StartWatchTaskExecuteTime();
                    RemoveRealChatWindowDto(weChatWindows.Select(item => item.Name).ToList());

                    foreach (var current in weChatWindows)
                    {
                        var currentRealWindowDto = GetRealChatWindowDto(current);
                        WeChatMessageManager(current, currentRealWindowDto);
                    }
                    EndWatchTaskExecuteTime();
                    Console.WriteLine("执行时间"+ TaskExcuteTime+"秒");
                    Thread.Sleep(500);
                }
                
                TaskExecuteCompleted();
            }));
            th.Start();
        }

        /// <summary>
        /// 微信当前单独聊天窗口的消息管理
        /// </summary>
        /// <param name="current"></param>
        /// <param name="dto"></param>
        private void WeChatMessageManager(FlaUI.Core.AutomationElements.AutomationElement current, WeChatWindowsDto dto)
        {
            try
            {
                var chatList = current.FindFirstByXPath("/Pane[2]/Pane/Pane[2]/Pane/Pane/Pane[2]/Pane[1]/Pane/List");

                if (chatList == null)
                    return;
                var chatElementSource = chatList.FindAllChildren().ToList();

                if (dto.Init == false)
                {
                    #region 初始化第一次打开的聊天窗口记录
                    foreach (var chatItem in chatElementSource)
                    {
                        if (chatItem.ControlType != FlaUI.Core.Definitions.ControlType.ListItem)
                            continue;

                        WeChatContractMessageDto currentMessage = null;

                        if (chatItem.Name != "[图片]" && chatItem.Name != "[动画表情]")
                            currentMessage = BuildChatMessage(chatItem);

                        if (currentMessage == null || currentMessage.UserType == 3)
                            continue;
                        var preMessage = dto.Messages.Count > 0 ? dto.Messages[dto.Messages.Count - 1] : null;
                        //如果是一个人连续发送多条重复记录则忽略本条
                        if (WeChatContractMessageDto.Equals(preMessage, currentMessage))
                            continue;
                        //如果未初始化则全部添加到原始集合中
                        currentMessage.PreMessage = preMessage;//上一条记录 构造消息链
                        dto.Messages.Add(currentMessage);
                        currentMessage.ExecuteStatus = MessageExecuteStatus.Complete;
                        //ExecuteWeChatMessageRemind(dto, currentMessage);  ////最开始的要保留也可以
                        currentMessage.ExecuteStatus = MessageExecuteStatus.Complete;
                    }
                    dto.Init = true;//设置为初始化完毕
                    #endregion
                }
                else
                {
                    //分析最近15条记录 ,如果不保险则加大该数值

                    #region 构造最近15条有效记录作为比较的依据

                    List<WeChatContractMessageDto> tempSource = new List<WeChatContractMessageDto>();
                    var cnt = chatElementSource.Count - 1;

                    for (int i = cnt; i >= 0; i--)
                    {
                        var chatItem = chatElementSource[i];

                        WeChatContractMessageDto currentMessage = null;

                        if (chatItem.BoundingRectangle.Bottom == 0 &&
                            chatItem.BoundingRectangle.Height == 0 && chatItem.BoundingRectangle.Width == 0 &&
                            chatItem.BoundingRectangle.Top == 0)
                            continue;
                        if (chatItem.Name == "以下是新消息")
                            continue;
                        if (chatItem.Name != "[图片]" && chatItem.Name != "[动画表情]")
                        {
                            currentMessage = BuildChatMessage(chatItem);
                        }
                        if (currentMessage == null || currentMessage.UserType == 3)
                            continue;

                        if (tempSource.Count >= 15)
                            break;

                        var temp = tempSource.Count > 0 ? tempSource[tempSource.Count - 1] : null;

                        if (!WeChatContractMessageDto.Equals(currentMessage, temp))
                        {
                            if (temp != null)
                                temp.PreMessage = currentMessage;
                            tempSource.Add(currentMessage);
                        }

                    }
                    tempSource.Reverse();
                    #endregion

                    #region 分析最近15条记录-如果不存在则视为新消息，如果最近3条记录不匹配也视为新消息

                    foreach (var ansyTemp in tempSource)
                    {
                        //比较最近三条记录链路 如果相等则代表消息是历史消息
                        var hisEqualMsg = dto.Messages.Where(s => s.UserName == ansyTemp.UserName && s.Message == ansyTemp.Message);
                        if (hisEqualMsg == null)
                        {
                            ansyTemp.PreMessage = dto.Messages.Count > 0 ? dto.Messages[dto.Messages.Count - 1] : null;
                            dto.Messages.Add(ansyTemp);
                            //执行新消息记录
                            ExecuteWeChatMessageRemind(dto, ansyTemp);
                            Repay(current, ansyTemp);
                            ansyTemp.ExecuteStatus = MessageExecuteStatus.Complete;
                        }
                        else
                        {
                            var newMessage = true;
                            foreach (var hisMsg in hisEqualMsg.ToList())
                            {
                                //递归比较
                                var result = MsgEqual(ansyTemp, hisMsg);

                                if (result)
                                    newMessage = false;
                            }
                            if (newMessage)
                            {
                                ansyTemp.PreMessage = dto.Messages.Count > 0 ? dto.Messages[dto.Messages.Count - 1] : null;
                                Repay(current, ansyTemp);
                                dto.Messages.Add(ansyTemp);
                                //执行新消息记录并回复
                                //AutomationElement fr = com.get_from_byname("文件传输助手");
                                //if (fr != null)
                                //{
                                //    Repay(fr, ansyTemp);
                                //}
                                ///////
                                ExecuteWeChatMessageRemind(dto, ansyTemp);
                                ansyTemp.ExecuteStatus = MessageExecuteStatus.Complete;
                            }

                        }
                    }
                    #endregion
                }
            }
            catch { }
        }

        private void Repay(FlaUI.Core.AutomationElements.AutomationElement current, WeChatContractMessageDto currentMsg)/////当前消息回复方法
        {
            try
            {
                if (currentMsg.UserName == "数字Ai助手")
                {
                    return;
                }
                if/* (currentMsg.ExecuteStatus == MessageExecuteStatus.Untreated) //&& */(currentMsg.Message.Contains("AI") || currentMsg.Message.Contains("助手"))
                {
                    current.Focus();
                    Thread.Sleep(10);
                        string re_str = "";
                        if (currentMsg.Message.Contains("@数字Ai助手"))
                        {
                            int index = currentMsg.Message.IndexOf("@数字Ai助手");

                            string result = currentMsg.Message.Substring(index + ("@数字Ai助手").Length);

                    //#region 转发到其他群
                    //com.send_mes_byname("AI测试群", "0", result);
                    //com.send_mes_byname("数字Ai助手", "0", result);
                    //#endregion

                    #region 获取智谱AI数据/营销
                    //if (currentMsg.Message.Contains("详细"))
                    //{
                    //    re_str = com.HttpPost(result);
                    //    com.send_mes_byname(current.Name, "0", re_str);
                    //}
                    //else
                    //{
                    //    re_str = com.HttpPost(result + "  100个字内回答");
                    //    com.send_mes_byname(current.Name, "0", re_str);   ////本身窗口回复
                    //}
                        re_str = com.HttpPost_yx(result);
                        if (re_str != null && re_str != "")
                        {
                            if (re_str.Substring(0, 2) == "No")
                            {
                                re_str = "您的问题，我回答不了，请查阅电力发布的相关资料";
                            }
                            com.send_mes_byname(current.Name, "0", re_str);
                        }                        
                        #endregion
                    }

                    //if (current.Name == "磁灶抢修群")
                    //    {
                    //        com.send_mes_byname("AI测试群", "0", "以下内容来自企业微信群<磁灶抢修群>的转发：<br>" + currentMsg.UserName+"："+currentMsg.Message);
                    //    }
                    //    if (current.Name == "数字办（信通运检)")
                    //    {
                    //        com.send_mes_byname("数字办（信通运检）工作群", "0", "以下内容来自企业微信群<数字办（信通运检)>的转发：<br>" + currentMsg.UserName + "：" + currentMsg.Message);
                    //}
                }
            }
            catch(Exception ex)
            {
                DataLink.Writelog(ex.ToString());
            }
        }

        private bool MsgEqual(WeChatContractMessageDto newMsg, WeChatContractMessageDto oldMsg)
        {
            var temp = WeChatContractMessageDto.Equals(newMsg, oldMsg);
            if (temp == false)
                return false;
            var preNewMsg = newMsg.PreMessage;
            var preOldMsg = oldMsg.PreMessage;
            while (true)
            {
                if (preNewMsg == null )
                {
                    break;
                }
                var result = WeChatContractMessageDto.Equals(preNewMsg, preOldMsg);
                 preNewMsg = preNewMsg?.PreMessage;
                 preOldMsg = preOldMsg?.PreMessage;
                if (result == false)
                    return false;
            }
            return true;
        }

        private WeChatContractMessageDto BuildChatMessage(FlaUI.Core.AutomationElements.AutomationElement item)
        {
            var my = item.FindFirstByXPath("/Pane/Pane[2]/Pane/Pane/Pane/Text");
            var to = my == null ? item.FindFirstByXPath("/Pane/Pane[1]/Pane/Pane/Pane/Text") : null;

            if (my != null)
            {
                var button = item.FindFirstByXPath("/Pane/Button");
                var map = button.Capture();
                return new WeChatContractMessageDto { UserHeader = map, Message = my.Name.ToString(), UserName = button.Name.ToString(), UserType = 1 };

            }
            else if (to != null)
            {
                var button = item.FindFirstByXPath("/Pane/Button");
                var map = button.Capture();
                return new WeChatContractMessageDto {  UserHeader= map, Message = to.Name.ToString(), UserName = button.Name.ToString(), UserType = 2 };
            }
            //else
            //{
            //    var time = item.FindFirstByXPath("/Text");//日期
            //    if (time != null)
            //    {
            //        var s = DateTime.Now;
            //        DateTime.TryParse(item.Name, out s);
            //        return new WeChatContractMessageDto { Time = s == DateTime.MinValue ? DateTime.Now : s, Message = time.Name.ToString(), UserType = 3 };
            //    }
            //}
            return null;
        }

        private WeChatContractMessageDto BuildImageMessage(FlaUI.Core.AutomationElements.AutomationElement item)
        {
            var my = item.FindFirstByXPath("/Pane/Button");
            //var to = item.FindFirstByXPath("/Pane/Pane[1]/Pane/Pane/Pane/Text");
            var image = item.FindFirstByXPath("/Pane/Pane[1]");
            if (my != null)
            {
                return new WeChatContractMessageDto { MessageType = 1, UserName = my.Name, Message = "[图片]", UserType = 2 };
            }
            return null;

        }

        private WeChatContractMessageDto BuildGifMessage(FlaUI.Core.AutomationElements.AutomationElement item)
        {
            var my = item.FindFirstByXPath("/Pane/Button");
            //var to = item.FindFirstByXPath("/Pane/Pane[1]/Pane/Pane/Pane/Text");
           // var image = item.FindFirstByXPath("/Pane/Pane[1]");
            if (my != null)
            {

                return new WeChatContractMessageDto { MessageType = 2, UserName = my.Name, Message = item.Name , UserType = 2 };
            }
            return null;

        }

        /// <summary>
        /// 如果内存存在窗体不存在则移除
        /// </summary>
        /// <param name="weChatWindows"></param>
        private void RemoveRealChatWindowDto(List<string> weChatWindows)
        {
            List<WeChatWindowsDto> removeSource = new List<WeChatWindowsDto>();
            foreach (var item in Reals)
            {
                if (weChatWindows.Count(s => s == item.ChatUser) <= 0)
                {
                    removeSource.Add(item);
                }
            }
            foreach (var item in removeSource)
            {
                Reals.Remove(item);
                ExecuteRemoveWindowReal(item);
            }
        }

        /// <summary>
        /// 实时聊天窗体管理事件传输
        /// </summary>
        /// <param name="current"></param>
        /// <returns></returns>
        private WeChatWindowsDto GetRealChatWindowDto(FlaUI.Core.AutomationElements.AutomationElement current)
        {
            var currentRealWindowDto = Reals.FirstOrDefault(real => real.ChatUser == current.Name);

            if (currentRealWindowDto == null)
            {
                currentRealWindowDto = new WeChatWindowsDto() { ChatUser = current.Name };
                Reals.Add(currentRealWindowDto);
                ExecuteAddWindowReal(currentRealWindowDto);
            }
            return currentRealWindowDto;
        }

        public override void Stop()
        {
            base.Stop();
        }
    }

    /// <summary>
    /// 用户正对微信单开的聊天窗体操作事件(打开和关闭）
    /// </summary>
    public class WeChatWindowHandlerEventArgs : EventArgs
    {
        public WeChatWindowsDto ChatWindowDto { get; set; }
    }
    /// <summary>
    /// 新消息提醒
    /// </summary>
    public class WeChatMessageRemindEventArgs : EventArgs
    {
        public WeChatWindowsDto ChatWindowDto { get; set; }

        public WeChatContractMessageDto NewMessage { get; set; }

    }
}
