﻿using FlaUI.Core.AutomationElements;
using FlaUI.Core.Input;
using FlaUI.Core.WindowsAPI;
using FlaUI.UIA3;
using Interop.UIAutomationClient;
using OnlineRetailers.Extension.Page.UIAuto.Business;
using OnlineRetailers.Extension.Page.UIAuto.Business.WIN;
using OnlineRetailers.Extension.WXAuto.Page.MainPage;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace OnlineRetailers.Extension.Page.UIAuto
{
	public class UI_WX_Window
	{
		[DllImport("User32.dll", CharSet = CharSet.Auto)]
		public static extern int GetWindowThreadProcessId(IntPtr hwnd, out int ID);

		[DllImport("user32.dll", EntryPoint = "FindWindow")]
		private extern static IntPtr FindWindow(string lpClassName, string lpWindowName);

		static UI_WX_Window weChat = null;

		private Window window = null;

		private string startPanel = "/Pane[3]";

		private UIA3Automation automation;
		private FlaUI.Core.Application application;
		public static UI_WX_Window Current
		{
			get
			{
				if (weChat == null)
				{
					weChat = new UI_WX_Window();
				}
				return weChat;
			}
		}
       private int weChatProcessID = 0;

        public FlaUI.Core.AutomationElements.Window Window { get => window; set => window = value; }

		/// <summary>
		/// 加载微信面板
		/// </summary>
		/// <returns></returns>
		public bool Load()
		{

			IntPtr hwnd = FindWindow(null, "微信");   ///查找微信窗口句柄

			if (hwnd != IntPtr.Zero)  
			{
				GetWindowThreadProcessId(hwnd, out weChatProcessID); ////通过句柄获取微信线程PID
			}
			else
			{
				return false;
			}
			var result = !Win32_IsWindowVisible.Do(WXWin32.GetWeiXin());
			if (result)   /////如果线程在后台，强制往前台
			{
				Win32_SetForegroundWindow.Do(WXWin32.GetWeiXin());
				Win32_ShowWindow.Normal(WXWin32.GetWeiXin());
			}
			application = FlaUI.Core.Application.Attach(weChatProcessID);

			automation = new UIA3Automation();

			Window = application.GetMainWindow(automation);  ////获取这个窗口的自动化界面

            startPanel = string.Format("/Pane[{0}]", Window.FindAllChildren().Length);

			return true;
		}
		/// <summary>
		/// 获取正在打开的聊天窗口(不包含主窗体)
		/// </summary>
		/// <returns></returns>
        public List<AutomationElement> GetWeChatWindows()
        {
			try
			{
				List<AutomationElement> weChat = new List<AutomationElement>();
				var source = WXWin32.GetAllDesktopWindows().ToList();
				foreach (var item in source)
				{
					if (item.szClassName == "ChatWnd")
					{
						var cc = automation.FromHandle(item.hWnd);
						if (cc.ControlType == FlaUI.Core.Definitions.ControlType.Window)
							weChat.Add(cc);
					}
				}

				//var source = application.GetAllTopLevelWindows(automation);

				//         foreach (var item in source)
				//         {
				//             if (item.Properties.ProcessId == weChatProcessID&&item.ClassName== "ChatWnd")
				//             {
				//                 weChat.Add(item);	
				//             }
				//         }
				return weChat;
			}
			catch(Exception ex)
			{
				return null;
			}
        }

        /// <summary>
        /// 获取微信从添加朋友按钮发起后的弹出面板 
        /// </summary>
        /// <returns></returns>
        public AutomationElement ShowFriendInfoPanel()
		{
			var allSource = automation.GetDesktop().FindAllChildren().ToList();

			foreach (var item in allSource)
			{
				if (item.Name == "微信")
				{
					if (item.ClassName == "ContactProfileWnd")
						return item;
				}
			}
			return null;
		}

		/// <summary>
		/// 获取添加朋友面板
		/// </summary>
		/// <returns></returns>
		public AutomationElement GetAddFriendWindow()
		{
			var fristElement = this.Window.FindAllChildren()[0];
			if (fristElement.Name == "添加朋友请求"
				&& fristElement.ClassName == "WeUIDialog" && fristElement.ControlType == FlaUI.Core.Definitions.ControlType.Window)
			{
				return fristElement;
			}

			return null;
		}

		public List<AutomationElement> GetAllWindow()
		{
			List<AutomationElement> source = new List<AutomationElement>();
			foreach (var item in application.GetAllTopLevelWindows(automation).ToList())
			{
				source.Add(item);
			};
			return source;
		}


		/// <summary>
		/// 主窗体获取焦点
		/// </summary>
		public void Focus()
		{
			var result = !Win32_IsWindowVisible.Do(WXWin32.GetWeiXin());
			if (result)
			{
				Win32_SetForegroundWindow.Do(WXWin32.GetWeiXin());
				Win32_ShowWindow.Normal(WXWin32.GetWeiXin());
			}
			if (window.AsWindow().Patterns.Window.PatternOrDefault != null)
			{
				window.AsWindow().Patterns.Window.Pattern.SetWindowVisualState(FlaUI.Core.Definitions.WindowVisualState.Normal);
			}
		}

		//var max = Find("/Pane[2]/Pane[3]/ToolBar/Button[3]");
		//if (max != null)
		//{
		// //max.AsButton().IsOffscreenInvoke();
		//}
		//this.window.Focus();
		//window.FocusNative();
		//IntPtr hwnd = FindWindow(null, "微信");
		//Business.WIN.WindowMax.Max(hwnd);

		public string WXName()
		{
			var item = Find("/Pane[2]/Pane[1]/Button[1]");

			if (item != null)
				return item.Name;
			return string.Empty;
		}

		/// <summary>
		/// 模拟点击点击元素
		/// </summary>
		/// <param name="ele"></param>
		public void ClickElement(AutomationElement ele)
		{
			if (ele != null)
			{
				Log.SystemLog.Info("开始执行元素【" + ele.Name + "】点击事件");
				WXWindowMessage.Click(ele);
			}
			else
			{
				Log.SystemLog.Error("元素为空，无法执行点击事件");
			}
		}

		/// <summary>
		/// 根据路径发现
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public AutomationElement Find(string path)
		{
			if (UI_WX_Window.Current.Window == null)
			{
				Log.SystemLog.Error("未发现微信窗口");

				return null;
			}
			startPanel = string.Format("/Pane[{0}]", Window.FindAllChildren().Length);
			string absPath = startPanel + path;
			var ele = UI_WX_Window.Current.Window.FindFirstByXPath(absPath);

			if (ele == null)
			{
				Log.SystemLog.Error("此路径" + absPath + "元素为空");
			}

			return ele;
		}

		/// <summary>
		/// 搜索群的情况下
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public AutomationElement FindGroup(string path)
		{
			if (UI_WX_Window.Current.Window == null)
			{
				Log.SystemLog.Error("未发现微信窗口");

				return null;
			}
			string absPath = "/Pane[1]" + path;
			var ele = UI_WX_Window.Current.Window.FindFirstByXPath(absPath);

			if (ele == null)
			{
				Log.SystemLog.Error("此路径" + absPath + "元素为空");
			}
			return ele;
		}

		/// <summary>
		/// 清除输入框的内容
		/// </summary>
		public void ClearInput()
		{
			Log.SystemLog.Info("执行清空文本框内容操作");
			Keyboard.Pressing(VirtualKeyShort.CONTROL);
			Keyboard.Press(VirtualKeyShort.KEY_A);
			Keyboard.Release(VirtualKeyShort.CONTROL);
			Keyboard.Press(VirtualKeyShort.BACK);
		}
		/// <summary>
		/// 粘贴
		/// </summary>
		public void Paste()
		{
			Log.SystemLog.Info("执行粘贴操作");
			Keyboard.Pressing(VirtualKeyShort.CONTROL);
			Keyboard.Press(VirtualKeyShort.KEY_V);
			Keyboard.Release(VirtualKeyShort.CONTROL);
		}
	}
}
