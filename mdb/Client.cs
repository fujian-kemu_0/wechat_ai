﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.UIAuto.mdb
{

    #region 系统类参数
    public class system_cls
    {
        public string title = "数字办-微信AI机器人";
    }
    #endregion

    #region 窗口数据加载类
    public class Form_cls
    {
        public string group_id;  ////窗口ID值
        public string group_name; ///窗口名称
        public bool is_log=false;   /// 是否日志
        public bool is_sen=false;  //是否敏感字眼
        public bool is_ai = false;   //是否启动AI
        public List<string> sens_str;   ///敏感字眼数组
    }
    #endregion 

}
