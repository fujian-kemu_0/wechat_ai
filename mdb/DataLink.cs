﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;//
using System.Web;
using System.Net;
using System.IO;
using System.Data.SQLite;
using System.Data.SQLite.Linq;

namespace AllData
{
    public class DataLink
    {
        SqlConnection conn;
        SQLiteConnection sqliteCon;
        string strConn;

        public void open()
        {
            string strFilePath = @"Data Source=xiaoq.db";
            string strCon = strFilePath + ";Pooling=true;FailIfMissing=false";
            sqliteCon = new SQLiteConnection(strCon);
            sqliteCon.Open();
        }
        public DataSet getData(string strCmd)
        {
            open();  //打开数据库
            SQLiteDataAdapter da = new SQLiteDataAdapter(strCmd, sqliteCon);
            DataSet ds = new DataSet();  //填充容器
            da.Fill(ds);
            sqliteCon.Close();
            return (ds);
        }
        public void SetUpdate(string Cmd)
        {
            open();  //打开数据库
            SQLiteCommand cmd = new SQLiteCommand(Cmd, sqliteCon);
            cmd.ExecuteNonQuery();
            sqliteCon.Close();
        }
        public string GetResponseData(string JSONData, string Url)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(JSONData);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.Method = "POST";
            request.ContentLength = bytes.Length;
            request.ContentType = "json";
            Stream reqstream = request.GetRequestStream();
            reqstream.Write(bytes, 0, bytes.Length);
            //声明一个HttpWebRequest请求
            request.Timeout = 90000;
            //设置连接超时时间
            request.Headers.Set("Pragma", "no-cache");
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream streamReceive = response.GetResponseStream();
            Encoding encoding = Encoding.UTF8;
            StreamReader streamReader = new StreamReader(streamReceive, encoding);
            string strResult = streamReader.ReadToEnd();
            streamReceive.Dispose();
            streamReader.Dispose();
            return strResult;
        }

        public class WXApi
        {
            public string access_token { set; get; }
        }
        #region 采集日志文件写入部分
        public static void Writelog(string msg)   /////在服务器生成日志文件
        {
            string path = Environment.CurrentDirectory;   ///////////当前程序的运行路径
            if (!path.EndsWith("\\")) path += "\\";
            string logPath = path + "log\\" + DateTime.Now.ToString("yyyy-MM-dd") + ".txt";
            try
            {
                using (StreamWriter sw = File.AppendText(logPath))
                {
                    sw.WriteLine("消息：" + msg);
                    sw.WriteLine("时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    sw.WriteLine("**************************************************");
                    sw.WriteLine();
                    sw.Flush();
                    sw.Close();
                    sw.Dispose();
                }
            }
            catch (IOException e)
            {
                using (StreamWriter sw = File.AppendText(logPath))
                {
                    sw.WriteLine("异常：" + e.Message);
                    sw.WriteLine("时间：" + DateTime.Now.ToString("yyy-MM-dd HH:mm:ss"));
                    sw.WriteLine("**************************************************");
                    sw.WriteLine();
                    sw.Flush();
                    sw.Close();
                    sw.Dispose();
                }
            }
        }
        #endregion
    }
}