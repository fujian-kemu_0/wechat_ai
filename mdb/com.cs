﻿using AllData;
using FlaUI.Core.AutomationElements;
using FlaUI.WinForm.Page.MainPage;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OnlineRetailers.Extension.Page.UIAuto;
using OnlineRetailers.Extension.Page.UIAuto.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.UIAuto.mdb;


namespace FlaUI.WinForm.UIAuto.Business.WX.Task.Base
{
    public class com
    {
        public static string get_group_id_byname(string name)
        {
            for (int i = 0; i < WeCharRealMessage.form_list.Count; i++)
            {
                if (WeCharRealMessage.form_list[i].group_name == name)
                {
                    return WeCharRealMessage.form_list[i].group_id;
                }
            }
            return "";
        }
        public static Form_cls get_group_oject_byname(string name)
        {
            for (int i = 0; i < WeCharRealMessage.form_list.Count; i++)
            {
                if (WeCharRealMessage.form_list[i].group_name == name)
                {
                    return WeCharRealMessage.form_list[i];
                }
            }
            return null;
        }
        public static AutomationElement get_from_byname(string name)  ////通过窗口名称获取窗体类
        {
            var weChatWindows = UI_WX_Window.Current.GetWeChatWindows();
            foreach (var fr in weChatWindows)
            {
                if (fr.Name == name)
                {
                    return fr;
                }
            }
            return null;
        }
        public static void send_mes_byname(string frm_name, string ty, string msg)  ////给某个窗体发送信息数据
        {
            AutomationElement fr = get_from_byname(frm_name);
            fr.Focus();////得到焦点
            Thread.Sleep(5);
            var edit = fr.FindFirstByXPath("/Pane[2]/Pane/Pane[2]/Pane/Pane/Pane[2]/Pane[2]/Pane[2]/Pane/Pane[1]/Edit");   ////得到输入框口XPath
            if (ty == "0")////信息
            {
                UI_WX_Element_Base_Task.ClickSetInputValue(new WeChatInputMsgEntity { Info = msg, Type = WXMessageType.Text }, edit, true, true);   ///发送文本
            }
            else if (ty == "1")
            {
                UI_WX_Element_Base_Task.ClickSetInputValue(new WeChatInputMsgEntity { Info = "C:\\pic\\repay_pic\\wait.png", Type = WXMessageType.Image }, edit,true,true);   ///发送图片
            }
            else if (ty == "2")
            {
                UI_WX_Element_Base_Task.ClickSetInputValue(new WeChatInputMsgEntity { Info = "C:\\xiaoq.txt", Type = WXMessageType.File }, edit, true, true);   ///发送文件   
            }
            var send = fr.FindFirstByXPath("/Pane[2]/Pane/Pane[2]/Pane/Pane/Pane[2]/Pane[2]/Pane[2]/Pane/Pane[2]/Pane[3]/Button");   ////得到发送按钮
            if (send != null)
                send.Click();
        }


        public static string HttpPost(string str)
        {
            try
            {
                JObject data = new JObject();
                data.Add("model", "glm-4");
                JObject message = new JObject();
                JArray message_j = new JArray();
                message.Add("role", "user");
                message.Add("content", str);
                message_j.Add(message);
                data.Add("messages", message_j);
                //创建http请求
                HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create("https://open.bigmodel.cn/api/paas/v4/chat/completions");
                //字符串转换为字节码
                byte[] bs = Encoding.UTF8.GetBytes(data.ToString());
                //参数类型，这里是json类型
                //还有别的类型如"application/x-www-form-urlencoded"
                httpWebRequest.ContentType = "application/json;";
                //参数数据长度
                httpWebRequest.ContentLength = bs.Length;
                //设置请求类型
                httpWebRequest.Method = "POST";
                var apiKey = "6f91cf9324aaa0f503b80af6ae871751.Y2Ul52EbRGZM3rTd";
                httpWebRequest.Headers.Add("Authorization", $"Bearer {apiKey}");
                //设置超时时间
                httpWebRequest.Timeout = 20000;

                //将参数写入请求地址中
                httpWebRequest.GetRequestStream().Write(bs, 0, bs.Length);
                //发送请求
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                //读取返回数据
                StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8);
                string responseContent = streamReader.ReadToEnd();
                streamReader.Close();
                httpWebResponse.Close();
                httpWebRequest.Abort();
                JObject jo = (JObject)JsonConvert.DeserializeObject(responseContent);
                JArray ja = (JArray)JsonConvert.DeserializeObject(jo["choices"].ToString());
                JObject jo2 = (JObject)JsonConvert.DeserializeObject(ja[0]["message"].ToString());
                return jo2["content"].ToString();
            }
            catch (Exception ex) 
            {
                DataLink.Writelog(ex.ToString());
                return null;
            }
        }
        public static string HttpPost_yx(string str)
        {
            try
            {
                //JObject data = new JObject();
                //data.Add("messages", str);
                string jsonBody = "{\"message\":\"" + str + "\"}";
                byte[] bs = Encoding.UTF8.GetBytes(jsonBody);
                //创建http请求
                HttpWebRequest httpWebRequest = (HttpWebRequest)HttpWebRequest.Create("https://newmeet.xiptai.com.cn/chat");
                //字符串转换为字节码
                //byte[] bs = Encoding.UTF8.GetBytes(data.ToString());
                //参数类型，这里是json类型
                //还有别的类型如"application/x-www-form-urlencoded"
                httpWebRequest.ContentType = "application/json;";
                //参数数据长度
                httpWebRequest.ContentLength = bs.Length;
                //设置请求类型
                httpWebRequest.Method = "POST";
                var appuser = "JJ_Test";
                var apiKey = "XIPT-the-Best";
                httpWebRequest.Headers.Add("X-Username", $"{appuser}");
                httpWebRequest.Headers.Add("X-Api-Key", $"{apiKey}");
                //设置超时时间
                httpWebRequest.Timeout = 20000;
                ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;

                //将参数写入请求地址中
                httpWebRequest.GetRequestStream().Write(bs, 0, bs.Length);
                //发送请求
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                //读取返回数据
                StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream(), Encoding.UTF8);
                string responseContent = streamReader.ReadToEnd();
                streamReader.Close();
                httpWebResponse.Close();
                httpWebRequest.Abort();
                JObject jo = (JObject)JsonConvert.DeserializeObject(responseContent);

                if (jo["ok"].ToString() == "True")
                {
                    return jo["response"].ToString();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                DataLink.Writelog(ex.ToString());
                return null;
            }
        }
    }
}
